import React from 'react';

import Items from '../item/items';
import Modal from '../modal/mod';

function MainPage(props){
    return (
    <>
        <div className='backgr'>
            <Items />
            <Modal modalMain={"Yes"}/>
        </div>
    </>
    )
}

export default MainPage