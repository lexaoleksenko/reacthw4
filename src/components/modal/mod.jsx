import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ButtonModCls from './buttonModCls';
import ButtonModEnt from './buttonModEnt';

import { 
    addItemsCart,
    toggleModal,
    itemDelCart,
    deleteItemCart,
    deleteItemId, 
} from '../../redux/actions';

function Modal(props){
    const dispatch = useDispatch();

    const modalState = useSelector(state => {
        const {modalReducer} = state
        return modalReducer.activeModal
    })

    const currentItem = useSelector(state => {
        // console.log("CurrentItem STATE>>>>>", state)
        const {cartReducer} = state
        return cartReducer.itemCart
    })

    const hendleMainEnter = () =>{
        dispatch(toggleModal());
        dispatch(addItemsCart());
    }

    const hendleMainClose = () =>{
        dispatch(toggleModal());
        dispatch(itemDelCart());
    }

    const hendleCartEnter = () =>{
        dispatch(toggleModal());
        dispatch(deleteItemCart());
    }

    
    const hendleCartClose = () =>{
        dispatch(toggleModal());
        dispatch(deleteItemId());
    }

    return (
        <div className='modal' style={{display: modalState ? "flex" : "none"}}>
        <div className='modOver' onClick={hendleMainClose}></div>
        <div className='modWind'>
            <div className='modHeader'>
                <div className='headerTitle'>Confirm your action!</div>
            </div>
            {props.modalMain ?
            <>
                <div className='modText'>{`Do you want to add item ${currentItem.name} with price ${currentItem.price} to cart`}</div>
                <div className='modFooter'>
                    <ButtonModCls onCloseClick={hendleMainClose}/>
                    <ButtonModEnt onEnterClick={hendleMainEnter} btnBgCol="#62d44d" btnCol="#FFFFFF"/>
                </div>
            </> : ""}
            {props.modalCart ?
            <>
                <div className='modText'>{`Do you want to delete this item?`}</div>
                <div className='modFooter'>
                    <ButtonModCls onCloseClick={hendleCartClose}/>
                    <ButtonModEnt onEnterClick={hendleCartEnter} btnBgCol="#d53e07" btnCol="#FFFFFF"/>
                </div>
            </> : ""}
            
        </div>
        </div>
    )
}

export default Modal