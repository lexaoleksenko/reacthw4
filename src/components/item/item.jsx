import React, { useState, useEffect} from 'react';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleXmark} from "@fortawesome/free-solid-svg-icons";
import PropTypes from 'prop-types';

import { useSelector, useDispatch } from 'react-redux';

import { 
    toggleModal,
    addItemCart,
    addItemsCart,
    deleteItemCartAdd,
    addItemsSelect,
    itemDelSelect,
    starStateTrue,
    starStateFalse, 
} from '../../redux/actions';

function Item(props){
    const dispatch = useDispatch();
    const [starState, setStarState] = useState(false)

    const hendleAddItem = () =>{
        dispatch(toggleModal());
        dispatch(addItemCart(props.item));
    }

    const hendleDeleteItem = () =>{
        dispatch(toggleModal());
        dispatch(deleteItemCartAdd(props.item.article));
    }

    const hendleAddSelect = () =>{
        const localState = localStorage.getItem(`starState${props.item.article}`)? JSON.parse(localStorage.getItem(`starState${props.item.article}`)) : undefined;
        if(localState === undefined){
            dispatch(addItemsSelect(props.item));
            dispatch(starStateTrue(props.item.article));
        } else if (localState){
            dispatch(starStateFalse(props.item.article))
            dispatch(itemDelSelect(props.item.article));
        }
    }

    const hendleDeleteSelect = () =>{
        dispatch(itemDelSelect(props.item.article));
        dispatch(starStateFalse(props.item.article))
    }

    useEffect(()=>{
        const localState = localStorage.getItem(`starState${props.item.article}`)? JSON.parse(localStorage.getItem(`starState${props.item.article}`)) : undefined;
        if(localState === undefined){
            setStarState(false)
        } else if (localState){
            setStarState(true)
        }
    })

    const itemsSelect = useSelector(state => {
        const {selectReducer} = state;
        return selectReducer.itemsSelect
    })

    const itemsCart = useSelector(state => {
        const {cartReducer} = state;
        return cartReducer.itemsCart
    })


    useEffect(()=>{
        const itmLoadSel = itemsSelect.find(el => el.article === props.item.article)
        const localState = localStorage.getItem(`starState${props.item.article}`)? JSON.parse(localStorage.getItem(`starState${props.item.article}`)) : undefined;
        if(localState === undefined){
            setStarState(false)
        } else if (localState && itmLoadSel === undefined){
            setStarState(true)
            dispatch(addItemsSelect(props.item));
        } else if (itmLoadSel){
            setStarState(true)
        }

        const itmLoadCart = itemsCart.find(el => el.article === props.item.article)
        const localCart = localStorage.getItem(`cartItem${props.item.article}`)? JSON.parse(localStorage.getItem(`cartItem${props.item.article}`)) : undefined;
        if(localCart === undefined){
            console.log("Local Storage Cart Clear")
        } else if (localCart && itmLoadCart === undefined){
            dispatch(addItemCart(props.item));
            dispatch(addItemsCart());
        } else if (itmLoadCart){
            console.log("Local Storage Cart Done")
        }
    }, [])

    return (
        <>
            <div className='items__item'>
                <h2 className='items__item_name'>{props.item.name}</h2>
                <img className='items__item_img' src={props.item.img} alt="#" />
                <span className='items__item_price'>{props.item.price}$</span>
                {props.itemMain ? 
                    <div>
                        <button className='items__item_btn' onClick={hendleAddItem}>Add To Cart</button>
                        <FontAwesomeIcon className='items__item_star' icon={faStar} onClick={hendleAddSelect} style={{color: starState ? "#FFFF00" : "#000000"}}/>
                    </div> : ""}
                {props.itemCart ? 
                    <div>
                        <FontAwesomeIcon className='items__item_star' icon={faCircleXmark} onClick={hendleDeleteItem}/>
                    </div> : ""}
                {props.itemSelect ? 
                    <div>
                        <FontAwesomeIcon className='items__item_star' icon={faStar} onClick={hendleDeleteSelect} style={{}}/>
                    </div> : ""}
            </div>
        </>
    )
}

Item.propTypes = {
    item : PropTypes.object,
    itemMain: PropTypes.string
}

Item.defaultProps = {
    item: {name: "item", article: "1", price: "price", img: "./phph.jpg"}
}
    
export default Item