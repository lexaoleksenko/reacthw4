import React from 'react';
import CartIco from './cartIco';
import SelectItem from './selectItmIco';
import { faStore } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {NavLink} from 'react-router-dom';

function Navbar(props){
    return (
        <>
            <ul className='navbar'>
                <li className="navbar__logo"><FontAwesomeIcon icon={faStore} className="navbar__logo_icon"  /><span className="navbar__logo_name">SuperShop</span></li>
                <NavLink to="/"><li className='navbar__home'>HOME</li></NavLink>
                <NavLink to="/cartPage"><li className='navbar__home'>CART</li></NavLink>
                <NavLink to="/selectPage"><li className='navbar__home'>SELECT</li></NavLink>
                <li className='navbar__select'><SelectItem /></li>
                <li className='navbar__cart'><CartIco /></li>
            </ul>
        </>
    )
}

export default Navbar