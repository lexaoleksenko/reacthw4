import React from 'react';
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { NavLink } from 'react-router-dom';

import {useSelector} from 'react-redux';

function CartIcon(props){

    const counterLength = useSelector(state => {
        const {cartReducer} = state
        return cartReducer.itemsCart.length
    })

    return (
        <>
        <NavLink to="/cartPage">
            <FontAwesomeIcon icon={faCartShopping} className='navbar__cart_icon'/>
        </NavLink>
        <div className='navbar__cart_counter'>{counterLength}</div>
        </>
    )
}

export default CartIcon