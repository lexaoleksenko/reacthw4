import React, {useEffect, useState} from 'react';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink } from 'react-router-dom';

import {useSelector} from 'react-redux';

function SelectItem(props){
    const[starState, setStarState] = useState(false)

    const counterLength = useSelector(state => {
        const {selectReducer} = state
        return selectReducer.itemsSelect.length
    })

    useEffect(()=>{
        if(counterLength === 0){
            setStarState(false)
        } else {
            setStarState(true)
        }
    }, [counterLength])

    return (
        <>
            <NavLink to="/selectPage"><FontAwesomeIcon icon={faStar} style={{color: starState ? "#FFFF00" : "#000000"}} className='navbar__select_icon'/></NavLink>
            <div className='navbar__select_counter'>{counterLength}</div>
        </>
    )
}

export default SelectItem