import React from 'react';
import { useSelector} from 'react-redux';

import Item from '../item/item';

function Cart(props){
    
    const cartItems = useSelector(state=>{
        // console.log("cartItems useSelector | STATE>>>>>", state)
        const {cartReducer} = state
        return cartReducer.itemsCart
    })

    return(
        <>
            <div className='items'>
                {!!cartItems.length && cartItems.map(el => {
                    return <Item key={el.article} item={el} itemCart="Yes"/>
                })}
            </div>
        </>
    )
}
export default Cart