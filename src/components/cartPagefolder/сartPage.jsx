import React from 'react';

import Cart from './cart';
import Modal from '../modal/mod';

function CartPage(props){
    
    return(
        <>
            <div className='backgr'>
                <Cart />
                <Modal modalCart={"Yes"}/>
            </div>
        </>
    )
}
export default CartPage