import {
    TOGGLE_MODAL,
    STAR_STATE_TRUE,
    ITEM_LOAD, 
    ITEM_ADD_CART, 
    ITEMS_ADD_CART,
    ITEM_DEL_CART,
    DELETE_ITEM_CARD,
    DELETE_ITEM_ADD,
    DELETE_ITEM_ID,
    ITEMS_ADD_SELECT,
    ITEM_DEL_SELECT,
    STAR_STATE_FALSE,
    LOADER_DISPLAY_OF,
    LOADER_DISPLAY_ON,
} from "./types"

export function toggleModal(){
    return{
        type: TOGGLE_MODAL
    }
};

export function starStateTrue(article){
    return{
        type: STAR_STATE_TRUE,
        article
    }
};

export function starStateFalse(article){
    return{
        type: STAR_STATE_FALSE,
        article
    }
};

export function loaderOn(){
    return{
        type: LOADER_DISPLAY_ON,
    }
}

export function loaderOff(){
    return{
        type: LOADER_DISPLAY_OF,
    }
}


export function itemLoad(){
    return async dispatch => {
        dispatch(loaderOn())
        const response = await fetch("https://dummyjson.com/products?limit=40");
        const jsonData = await response.json();
        dispatch({
            type: ITEM_LOAD,
            data: jsonData,
        })
        dispatch(loaderOff())        
    }
};

// CART

export function addItemCart(item){
    return{
        type: ITEM_ADD_CART,
        item
    }
};

export function itemDelCart(){
    return{
        type: ITEM_DEL_CART,
    }
};

export function addItemsCart(){
    return{
        type: ITEMS_ADD_CART,
    }
};

export function deleteItemCartAdd(id){
    return{
        type: DELETE_ITEM_ADD,
        id
    }
};

export function deleteItemCart(){
    return{
        type: DELETE_ITEM_CARD,
    }
};

export function deleteItemId(){
    return{
        type: DELETE_ITEM_ID,
    }
};

// SELECT

export function addItemsSelect(item){
    return{
        type: ITEMS_ADD_SELECT,
        item
    }
};

export function itemDelSelect(article){
    return{
        type: ITEM_DEL_SELECT,
        article
    }
};


