import {
    ITEM_LOAD,
    STAR_STATE_TRUE,
    STAR_STATE_FALSE,
} from "../types"

const initialState = {
    itemLoad: [],
    starState: [],
}

export const itemLoadReducer = (state = initialState, action) =>{
    // console.log("itemLoadReducer| STATE>>>",state,"|  ACTION>>>",action)

    switch(action.type){
        case ITEM_LOAD:
            const itemNew = action.data.products.map(el => {
                return{
                    name: el.title,
                    article: el.id,
                    img: "./phph.jpg",
                    price: el.price,
                }
            })
            return {
                ...state,
                itemLoad: itemNew
            }
        case STAR_STATE_TRUE:
            return (()=>{
                const {article} = action;
                const {itemLoad} = state;
                const newItem = itemLoad.find(el => el.article === article)
                localStorage.setItem(`starState${newItem.article}`, JSON.stringify(newItem));
                const newStarState = {
                    article: newItem.article
                    };
                return {
                    ...state,
                    starState: [...state.starState, newStarState]
                }
            })();
        case STAR_STATE_FALSE:
            return (()=>{
                const {article} = action;
                localStorage.removeItem(`starState${article}`);
                const {starState} = state;
                const itemIndex = starState.findIndex(res => res.article === article)
                const newStarState = [
                    ...starState.slice(0,itemIndex),
                    ...starState.slice(itemIndex +1)
                ];

                return {
                    ...state,
                    starState: newStarState
                }
            })();
        default:
            return state;
    }
}