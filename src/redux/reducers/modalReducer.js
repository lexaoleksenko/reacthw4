import { TOGGLE_MODAL } from "../types"

const initialState = {
    activeModal: false
}

export const modalReducer = (state = initialState, action) =>{
    // console.log("modalReducer| STATE>>>",state,"|  ACTION>>>",action)

    switch(action.type){
        case TOGGLE_MODAL:
            const {activeModal} = state
            return{
                ...state,
                activeModal: !activeModal
            }
        default:
            return state;
    }
}